#include <iostream>

using namespace std;

class Engine
{
    public:
        string checkIfStringIsValidOrNot(string str)
        {
            int paranthesesCount = 0;
            int len              = (int)str.length();
            for(int i = 0 ; i < len ; i++)
            {
                paranthesesCount += str[i] == '(' ? 1 : 0;
                paranthesesCount -= str[i] == ')' ? 1 : 0;
            }
            return (paranthesesCount ? "invalid" : "valid");
        }
};

int main(int argc, const char * argv[])
{
    Engine e = Engine();
    cout<<e.checkIfStringIsValidOrNot("((BCD)AE)")<<endl;
    return 0;
}
